const logInForm = document.querySelector('.log-in-form'),
    newToDoForm = document.querySelector('.new-todo-form'),
    toDoContainer = document.querySelector('.todo-container'),
    logOutBtn = document.querySelector('.log-out');

const loginURL = 'https://ajax.test-danit.com/api/v2/cards/login';
const cardsURL = "https://ajax.test-danit.com/api/v2/cards";
const TOKEN = 'token';
class ToDoItem {
    constructor(id, text) {
        this.id = id;
        this.text = text;
        this.container = document.createElement('div');
        this.deleteBtn = document.createElement('button');
    }
    createElements() {
        this.container.className = "todo";
        this.container.innerHTML = `<span>${this.text}</span>`
        this.deleteBtn.innerHTML = 'delete';
        this.container.append(this.deleteBtn);
        this.deleteBtn.addEventListener('click', () =>{
            axios.delete(`${cardsURL}/${this.id}`,{
                headers: {
                    Authorization: `Bearer ${localStorage.getItem(TOKEN)}`
                }
            }).then(({status}) => {
                if(status === 200) this.container.remove();
            });
        })
    }
    render(selector) {
        this.createElements();
        document.querySelector(selector).prepend(this.container);
    }
}

logInForm.addEventListener('submit', function (event) {
    event.preventDefault();
    const body = {}
    event.target.querySelectorAll('input').forEach(input => {
        body[input.name] = input.value;
    })
    // console.log(body)
    axios.post(loginURL, body).then(({ data }) => {
        localStorage.setItem(TOKEN, data)
        location.reload();
    }).catch(({ response }) => {
        alert(response.data)
    })
});
function successAuthorization() {
    logInForm.style.display = 'none';
    newToDoForm.style.display = 'block';
    toDoContainer.style.display = 'block';
    logOutBtn.style.display = 'block';
    axios.get(cardsURL, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem(TOKEN)}`
        }
    })
        .then(({ data }) => {
            data.forEach(({ id, todo }) => {
                new ToDoItem(id, todo).render('.todo-container')
            })
        }).catch(({ response }) => {
            alert(response.data)
        })
}
if (localStorage.getItem(TOKEN)) successAuthorization();
newToDoForm.addEventListener('submit', (event) => {
    event.preventDefault();
    const body = {}
    event.target.querySelectorAll('input').forEach(input => {
        body[input.name] = input.value;
    })
    axios.post(cardsURL, body, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem(TOKEN)}`
        }
    }).then(({ data }) => {
        new ToDoItem(data.id, data.todo).render('.todo-container')
        newToDoForm.reset();
     }).catch(({ response }) => {
        alert(response.data)
    });
})
logOutBtn.addEventListener('click', () =>{
    localStorage.removeItem(TOKEN);
    location.reload();
})

